import React from 'react';
import './Spinner.css';

function Spinner() {
  return (
    <div className="ui active dimmer inverted">
      <div className="ui loader massive"></div>
    </div>
  );
}

export default Spinner;
