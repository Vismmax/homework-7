import React from 'react';
import PropTypes from 'prop-types';
import './ChatPost.css';
import { TypePost } from '../../models/types';
import moment from 'moment';

type Props = {
  post: TypePost;
  right?: boolean;
  editPost: (post: TypePost) => void;
  deletePost: (post: TypePost) => void;
  likePost: (post: TypePost) => void;
};

function ChatPost({ post, right = false, editPost, deletePost, likePost }: Props) {
  return (
    <div className={`chat-post ui segment event ${right ? 'right' : ''}`}>
      {!right && (
        <div className="label">
          <img src={post.avatar} alt="avatar" />
        </div>
      )}
      <div className="content">
        <div className="summary">
          {!right && <a>{post.user}</a>}
          <div className="date">
            {moment(post.createdAt).calendar()}
            {post.editedAt ? ` (updated: ${moment(post.editedAt).calendar()})` : ''}
          </div>
        </div>
        <div className="extra text">{post.text}</div>
        <div className="meta">
          {!right && (
            <div>
              <a className={`like ${post.isLikes ? 'liked' : ''}`} onClick={() => likePost(post)}>
                <i className="like icon" />
              </a>
            </div>
          )}
          {right && (
            <div className="edit-actions">
              <i className="edit link icon" onClick={() => editPost(post)} />
              <i className="trash link icon" onClick={() => deletePost(post)} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

ChatPost.propTypes = {
  post: PropTypes.object.isRequired,
  right: PropTypes.bool,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
};

export default ChatPost;
