import { SET_USER } from './actionTypes';
import { TypeUser } from '../../models/types';
import { getNewUuid } from '../../services/uuid';

export type TypeUserAction = {
  type: string;
  payload: TypeUser;
};

export const setUser = (name: string) => ({
  type: SET_USER,
  payload: {
    id: name ? getNewUuid() : '',
    name,
  },
});
