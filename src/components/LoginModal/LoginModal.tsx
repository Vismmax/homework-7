import React, { FormEvent, useState } from 'react';
import './LoginModal.css';
import { useDispatch, useSelector } from 'react-redux';
import { TypeRootState } from '../../models/types';
import { setUser } from './actions';

function LoginModal() {
  const [userName, setUserName] = useState('');
  const currentUser = useSelector((state: TypeRootState) => state.currentUser);
  const dispatch = useDispatch();

  const onSubmitHandler = (ev: FormEvent) => {
    dispatch(setUser(userName));
    setUserName('');
    ev.preventDefault();
  };

  const loginModal = (
    <div className="login-modal ui dimmer modals visible active">
      <div className="ui tiny modal visible active">
        <div className="header">Login</div>
        <div className="content">
          <form className="ui form big" onSubmit={onSubmitHandler}>
            <div className="login-fields fields">
              <div className="field-name field">
                <label>User Name</label>
                <input
                  placeholder="User Name"
                  type="text"
                  value={userName}
                  onChange={(e) => setUserName(e.target.value)}
                />
              </div>
              <div className="field">
                <button className="ui button big" type="submit">
                  Login
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );

  return currentUser.id ? null : loginModal;
}

export default LoginModal;
