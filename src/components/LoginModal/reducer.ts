import { SET_USER } from './actionTypes';
import { TypeUser } from '../../models/types';
import { TypeUserAction } from './actions';

const initialState = {} as TypeUser;

export default function (state = initialState, action: TypeUserAction) {
  switch (action.type) {
    case SET_USER: {
      const currentUser = action.payload;
      return currentUser;
    }

    default:
      return state;
  }
}
