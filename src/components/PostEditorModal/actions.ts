import { SET_EDITABLE_POST, DROP_EDITABLE_POST } from './actionTypes';
import { TypePost } from '../../models/types';

export const setEditablePost = (post: TypePost) => ({
  type: SET_EDITABLE_POST,
  payload: post,
});

export const dropEditablePost = () => ({
  type: DROP_EDITABLE_POST,
});
