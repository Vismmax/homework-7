import { DROP_EDITABLE_POST, SET_EDITABLE_POST } from './actionTypes';

const initialState = null;

export default function (state = initialState, action: any) {
  switch (action.type) {
    case SET_EDITABLE_POST: {
      return action.payload;
    }
    case DROP_EDITABLE_POST: {
      return null;
    }

    default:
      return state;
  }
}
