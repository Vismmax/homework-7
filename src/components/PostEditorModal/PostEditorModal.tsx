import React, { useEffect, useState } from 'react';
import './PostEditorModal.css';
import { TypeRootState } from '../../models/types';
import { useDispatch, useSelector } from 'react-redux';
import { addPostAction, updatePostAction } from '../../containers/Chat/actions';
import { dropEditablePost } from './actions';

function PostEditorModal() {
  const [text, setText] = useState('');
  const currentUser = useSelector((state: TypeRootState) => state.currentUser);
  const editablePost = useSelector((state: TypeRootState) => state.editablePost);
  const dispatch = useDispatch();

  useEffect(() => {
    if (editablePost?.id) {
      setText(editablePost.text);
    } else {
      setText('');
    }
  }, [editablePost]);

  const save = () => {
    if (editablePost?.id) {
      dispatch(updatePostAction({ ...editablePost, text }));
    } else {
      dispatch(addPostAction(text, currentUser));
    }
    dispatch(dropEditablePost());
  };

  const close = () => {
    dispatch(dropEditablePost());
  };

  const editor = (
    <div className="login-modal ui dimmer modals visible active">
      <div className="ui tiny modal visible active">
        <div className="header">Edit Post</div>
        <div className="content">
          <div className="ui form">
            <textarea rows={3} value={text} onChange={(e) => setText(e.target.value)} />
          </div>
        </div>
        <div className="actions">
          <button className="ui button" onClick={close}>
            Cancel
          </button>
          <button className="ui button primary" onClick={save}>
            Update
          </button>
        </div>
      </div>
    </div>
  );

  return editablePost ? editor : null;
}

export default PostEditorModal;
