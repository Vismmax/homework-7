import React from 'react';
import './PageHeader.css';
import logo from '../../img/logo.svg';
import { useDispatch, useSelector } from 'react-redux';
import { TypeRootState } from '../../models/types';
import { setUser } from '../LoginModal/actions';

function PageHeader() {
  const userName = useSelector((state: TypeRootState) => state.currentUser.name);
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(setUser(''));
  };

  return (
    <div className="page-header ui text menu">
      <div className="item">
        <img src={logo} alt="Logo" />
      </div>
      <div className="header item right">{userName}</div>
      <i className="page-logout icon link sign-out" onClick={logout}></i>
    </div>
  );
}

export default PageHeader;
