import React from 'react';
import PropTypes from 'prop-types';
import './ChatHeader.css';
import moment from 'moment';

type Props = {
  chatName: string;
  countUsers: number;
  countPosts: number;
  dateLastPost: string;
};

function ChatHeader({ chatName, countUsers, countPosts, dateLastPost }: Props) {
  return (
    <div className="chat-header ui menu">
      <div className="header item">{chatName}</div>
      <div className="item">
        <div className="ui label blue">{countUsers} participants</div>
      </div>
      <div className="item">
        <div className="ui label blue">{countPosts} messages</div>
      </div>
      <div className="item right">
        <div className="ui label">
          last message at {moment(dateLastPost).format('MMMM Do YYYY, hh:mm:ss')}
        </div>
      </div>
    </div>
  );
}

ChatHeader.propTypes = {
  chatName: PropTypes.string.isRequired,
  countUsers: PropTypes.number.isRequired,
  countPosts: PropTypes.number.isRequired,
  dateLastPost: PropTypes.string.isRequired,
};

export default ChatHeader;
