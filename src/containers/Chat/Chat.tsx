import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './Chat.css';
import { TypePost, TypeRootState } from '../../models/types';
import { addPostAction, deletePostAction, updatePostAction, loadFakePostsAction } from './actions';
import Spinner from '../../components/Spinner/Spinner';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import ChatPosts from '../../components/ChatPosts/ChatPosts';
import ChatEditor from '../../components/ChatEditor/ChatEditor';
import PostEditorModal from '../../components/PostEditorModal/PostEditorModal';
import { setEditablePost } from '../../components/PostEditorModal/actions';

function Chat() {
  const [isLoading, setIsLoading] = useState(false);

  const currentUser = useSelector((state: TypeRootState) => state.currentUser);
  const posts = useSelector((state: TypeRootState) => state.posts);
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentUser.id) loadFakePosts();
  }, [currentUser]);

  useEffect(() => {
    document.addEventListener('keyup', keyUpHandler);
    return () => {
      document.removeEventListener('keyup', keyUpHandler);
    };
  }, [currentUser, posts]);

  const loadFakePosts = () => {
    setIsLoading(true);
    setTimeout(() => {
      dispatch(loadFakePostsAction());
      setIsLoading(false);
    }, 1000);
  };

  const keyUpHandler = (ev: KeyboardEvent) => {
    if (ev.key === 'ArrowUp') {
      const postsCurrentUser = posts.filter((post) => post.userId === currentUser.id);
      const post = postsCurrentUser[postsCurrentUser.length - 1];
      if (post) dispatch(setEditablePost(post));
    }
  };

  const countUsers = () => {
    const users = new Set(posts.map((post) => post.user));
    return users.size;
  };

  const addPost = (text: string) => {
    dispatch(addPostAction(text, currentUser));
  };

  const deletePost = (post: TypePost): void => {
    dispatch(deletePostAction(post));
  };

  const editPost = (post: TypePost): void => {
    dispatch(setEditablePost(post));
  };

  const likePost = (post: TypePost): void => {
    dispatch(
      updatePostAction({
        ...post,
        isLikes: !post.isLikes,
      }),
    );
  };

  return (
    <div className="chat ui container">
      <ChatHeader
        chatName={'Chat'}
        countUsers={countUsers()}
        countPosts={posts.length}
        dateLastPost={posts.length ? posts[posts.length - 1].createdAt : ''}
      />
      <ChatPosts
        posts={posts}
        currentUser={currentUser}
        editPost={editPost}
        deletePost={deletePost}
        likePost={likePost}
      />
      <ChatEditor onSubmit={addPost} />
      <PostEditorModal />
      {isLoading && <Spinner />}
    </div>
  );
}

export default Chat;
