import { ADD_POST, UPDATE_POST, DELETE_POST, LOAD_FAKE_POSTS } from './actionTypes';
import { TypePost, TypeUser } from '../../models/types';
import { getNewUuid } from '../../services/uuid';
import { getInitialPosts } from '../../services/data';

export type TypePostAction = {
  type: string;
  payload: TypePost;
};

export const addPostAction = (text: string, user: TypeUser): TypePostAction => ({
  type: ADD_POST,
  payload: {
    id: getNewUuid(),
    text: text,
    user: user.name,
    userId: user.id,
    avatar: '',
    createdAt: new Date().toJSON(),
    editedAt: '',
  },
});

export const updatePostAction = (post: TypePost) => ({
  type: UPDATE_POST,
  payload: { ...post, editedAt: new Date().toJSON() },
});

export const deletePostAction = (post: TypePost) => ({
  type: DELETE_POST,
  payload: post,
});

export const loadFakePostsAction = () => ({
  type: LOAD_FAKE_POSTS,
  payload: getInitialPosts(),
});
