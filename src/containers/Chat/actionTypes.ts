export const ADD_POST = 'ADD_POST';
export const UPDATE_POST = 'UPDATE_POST';
export const DELETE_POST = 'DELETE_POST';
export const LOAD_FAKE_POSTS = 'LOAD_FAKE_POSTS';
