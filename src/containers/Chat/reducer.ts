import { ADD_POST, DELETE_POST, LOAD_FAKE_POSTS, UPDATE_POST } from './actionTypes';
import { TypePostAction } from './actions';
import { TypePost } from '../../models/types';

const initialState = [] as TypePost[];

export default function (state = initialState, action: TypePostAction) {
  switch (action.type) {
    case ADD_POST: {
      const newPost = action.payload;
      return [...state, newPost];
    }

    case UPDATE_POST: {
      const updatedPost = action.payload;
      return state.map((user) => (user.id === updatedPost.id ? updatedPost : user));
    }

    case DELETE_POST: {
      const deletedPost = action.payload;
      return state.filter((user) => user.id !== deletedPost.id);
    }

    case LOAD_FAKE_POSTS: {
      return action.payload;
    }

    default:
      return state;
  }
}
