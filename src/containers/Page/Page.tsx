import React from 'react';
import './Page.css';
import PageHeader from '../../components/PageHeader/PageHeader';
import PageFooter from '../../components/PageFooter/PageFooter';
import Chat from '../Chat/Chat';
import LoginModal from '../../components/LoginModal/LoginModal';

function Page() {
  return (
    <div className="page ui">
      <PageHeader />
      <Chat />
      <PageFooter />
      <LoginModal />
    </div>
  );
}

export default Page;
