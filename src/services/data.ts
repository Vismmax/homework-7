import posts from '../data';

export const getInitialPosts = () => {
  return posts;
};

export default { getInitialPosts };
