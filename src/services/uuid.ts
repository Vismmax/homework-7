import { v4 as uuidv4 } from 'uuid';

export const getNewUuid = () => {
  return uuidv4();
};

export default { getNewUuid };
