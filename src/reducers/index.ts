import { combineReducers } from 'redux';
import posts from '../containers/Chat/reducer';
import currentUser from '../components/LoginModal/reducer';
import editablePost from '../components/PostEditorModal/reducer';

const rootReducer = combineReducers({
  posts,
  currentUser,
  editablePost,
});

export default rootReducer;
